import { DropdownDirective } from './dropdown.directive';
import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
//Outsourcen von Sachen, die nicht zur ganzen App gehören und nicht zum Feature gehören
@NgModule({
    declarations: [
        DropdownDirective,
        HomeComponent
    ],
    exports: [DropdownDirective, HomeComponent]
})
export class CoreModule {}
import { RecipeComponent } from './recipe.component';
import { RecipeDetailComponent } from './recipe-detail/recipe-detail.component';
import { RecipeEditComponent } from './recipe-edit/recipe-edit.component';
import { RecipeStartComponent } from './recipe-start.component';
import { Routes, RouterModule } from '@angular/router';


const RECIPE_ROUTES: Routes = [
    //Verschachtelte Routes (Kind Routes) dh /rezepte/children
    { path: '', component: RecipeComponent, children: [
        { path: '', component: RecipeStartComponent},
        { path: 'neu', component: RecipeEditComponent},
        { path: ':id', component: RecipeDetailComponent},
        { path: ':id/bearbeiten', component: RecipeEditComponent}
    ]},
    
];

export const recipeRouting = RouterModule.forChild(RECIPE_ROUTES);
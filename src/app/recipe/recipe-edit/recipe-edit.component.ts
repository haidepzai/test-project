import { Recipe } from './../recipe.model';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { RecipeService } from '../recipe.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styles: [
  ]
})
export class RecipeEditComponent implements OnInit, OnDestroy {
  recipeForm: FormGroup;
  private recipeIndex: number;
  private subscription: Subscription;
  private isNew = true;
  private recipe: Recipe;

  constructor(
    private recipeService: RecipeService,
    private router: Router,
    private route: ActivatedRoute
    ) { }

  getIngredientControls() {
    return (<FormArray>this.recipeForm.get('ingredients')).controls;
  }

  onSubmit(){
    const newRecipeTest = {'name' :"hallo",'description': "hi",'imagePath': "yo",'ingredients': [{'name': "hi",'amount': "ya"}]};
    //Da bei FromGroup der Name gleich ist mit dem Recipe Model, 
    //Kann man in dem Fall einfach die value vom recipeForm übernehmen,
    //anstatt new Recipe(this.recipeForm.get('name').value, this.recipeForm.get('imagePath'),...)
    //Formular hat ja auch Namen, Description, ImagePath und Ingredients!
    const newRecipe: Recipe = this.recipeForm.value; //Object (Holt die Daten vom Form)
    console.log(this.recipeForm);
    console.log(newRecipe);
    console.log(newRecipeTest);
    console.log(typeof(newRecipe));
    if(this.isNew){
      this.recipeService.addRecipe(newRecipe); //Neu
    } else {
      this.recipeService.editRecipe(this.recipe, newRecipe); //Überschreiben
    }    
    this.onNavigateBack();
  }

  onCancel(){
    this.onNavigateBack();
  }

  private onNavigateBack() {
    this.router.navigate(['/']);
  }

  onAddIngredientControl(name: string, amount: string){
    (<FormArray>this.recipeForm.get('ingredients')).push(
      new FormGroup({
        'name': new FormControl(name, Validators.required),
        'amount': new FormControl(amount, [Validators.required])
      })
    );
  }

  onRemoveIngredientControl(index: number){
    (<FormArray>this.recipeForm.get('ingredients')).removeAt(index);
  }

  ngOnInit(): void {
    this.subscription = this.route.params.subscribe(
      params => {
        if(params.hasOwnProperty('id')) { //Gibt es einen ID parameter?
          this.isNew = false;
          this.recipeIndex = +params['id']; //id in number
          this.recipe = this.recipeService.getRecipe(this.recipeIndex); //Rezept zuweisen
        } else {
          this.isNew = true;
          this.recipe = null;
        }
      }
    );

    let recipeName = null;
    let recipeImagePath = null;
    let recipeDescription = null;
    let recipeIngredients = new FormArray([]);

    //Edit Mode:
    if(!this.isNew){
      if (this.recipe.hasOwnProperty('ingredients')) {
        for(let ingredient of this.recipe.ingredients){
          recipeIngredients.push(
            new FormGroup({
              'name': new FormControl(ingredient.name, Validators.required),
              'amount': new FormControl(ingredient.amount, Validators.required)
            })
          )
        }
      }
      recipeName = this.recipe.name;
      recipeImagePath = this.recipe.imagePath;
      recipeDescription = this.recipe.description;
    }

    this.recipeForm = new FormGroup({
      'name': new FormControl(recipeName, Validators.required),
      'imagePath': new FormControl(recipeImagePath, Validators.required),
      'description': new FormControl(recipeDescription, Validators.required),
      'ingredients': recipeIngredients
      /*
      'ingredients': new FormArray([
        // new FormGroup({
        //   'name': new FormControl(null, Validators.required),
        //   'amount': new FormControl(null, Validators.required)
        // })
      ]),
      */
    });

  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}

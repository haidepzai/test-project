import { EventEmitter, Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { Recipe } from './recipe.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class RecipeService {
  databaseUrl: string = 'https://hai-recipe.firebaseio.com/';
  databaseEndpoint: string = 'recipe.json';
  recipesChanged = new EventEmitter<Recipe[]>();

  constructor(private http: HttpClient) {}

  private recipes: Recipe[] = [
    new Recipe(
      'Wiener Schnitzel',
      'mit Pommes',
      'https://cdn.gutekueche.de/upload/rezept/371/wiener-schnitzel.jpg',
      [new Ingredient('Pommes', '200g'), new Ingredient('Schnitzel vom Kalb', '250g')]
    ),
    new Recipe(
      'Salat',
      'Cäsar Style',
      'https://api-prod.sevencooks.com/img/general/gruener_salat_mit_joghurt_kurkuma_sauce_515743552-1022594-335-544-6.jpg',
      []
    ),
  ];

  //Wird nicht mehr benötigt seit Routing
  //recipeSelected = new EventEmitter<Recipe>();

  //Alle Rezepte bekommen
  getRecipes(): Recipe[] {
    return this.recipes;
  }

  //Ein Rezept bekommen, an der Stelle id
  getRecipe(id: number): Recipe {
    return this.recipes[id];
  }

  deleteRecipe(id: number): void {
    this.recipes.splice(id, 1);
  }

  addRecipe(recipe: Recipe): void {
    this.recipes.push(recipe);
  }

  editRecipe(oldRecipe: Recipe, newRecipe: Recipe): void {
    this.recipes[this.recipes.indexOf(oldRecipe)] = newRecipe; //Überschreiben
  }

  storeData():Observable<Object> {
    const body = JSON.stringify(this.recipes);
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put<Recipe[]>(this.databaseUrl + this.databaseEndpoint, body, {headers: headers});
  }

  fetchData():void {
    this.http.get<Recipe[]>(this.databaseUrl + this.databaseEndpoint).pipe(
      map((response) => response)
    ).subscribe(
      (recipes: Recipe[]) => {
        this.recipes = recipes;
        this.recipesChanged.emit(this.recipes);
      }
    );
  }
}

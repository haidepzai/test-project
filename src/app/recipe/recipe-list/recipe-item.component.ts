import { Recipe } from './../recipe.model';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styles: [
  ]
})
export class RecipeItemComponent implements OnInit {
  @Input() recipe: Recipe;
  //Bekommt vom Mutter die Daten (Recipe-list) (wird nicht mehr benötigt seit Service)
  //Statt Input könnte man hier auch direkt ein recipe = new Recipe(); erstellen
  //@Output() recipeSelected = new EventEmitter<Recipe>();
  //1. Mal übergeben an Recipe List
  @Input() recipeId: number;

  constructor(private recipeService: RecipeService) { }

  ngOnInit(): void {
  }

  /*
  War am Anfang da, um das Rezept-Detail anzuzeigen mit einem click Event
  //onSelect übergibt die Daten weiter (emit)
  onSelect() {
    //this.recipeSelected.emit(this.recipe);
    this.recipeService.recipeSelected.emit(this.recipe); 
    //emitte das Rezept an den Service (damit recipe-detail subscriben kann)
    //Davor musste man bis zu Recipe.component schicken und dieser schickte an recipe.detail
  }*/

}

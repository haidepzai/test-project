import { Recipe } from './../recipe.model';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Ingredient } from 'src/app/shared/ingredient.model';
import { RecipeService } from '../recipe.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styles: [
  ]
})
export class RecipeListComponent implements OnInit {
  //@Output() recipeSelected = new EventEmitter<Recipe>();
  //2. Mal übergeben an Mutter (recipe) verkettet.
  //(wird nicht mehr benötigt seit Service)
  
  recipes : Recipe[]; //davor waren die Dummy Items hier, jetzt durch Service

  constructor(
    private recipeService: RecipeService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.recipes = this.recipeService.getRecipes();
    //Auf das EventEmitter subscriben und so auf Veränderungen reagieren
    //(Wenn Rezept neu geladen wird)
    this.recipeService.recipesChanged.subscribe(
      (recipes: Recipe[]) => this.recipes = recipes
    )
  }

  onNewRecipe() {
    this.router.navigate(['/rezepte', 'neu']);
  }

  /*
  onSelected(recipe: Recipe){
    this.recipeSelected.emit(recipe);
  }
  */

}

import { ShoppingListService } from './../../shopping-list/shopping-list.service';
import { Recipe } from './../recipe.model';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { RecipeService } from '../recipe.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styles: [
  ]
})
export class RecipeDetailComponent implements OnInit, OnDestroy {
  //@Input() selectedRecipe: Recipe;
  //Bekommt Daten vom Recipe.component

  //Mit Services:
  selectedRecipe: Recipe;
  recipeId: number;

  private subscription: Subscription;

  constructor(
    private recipeService: RecipeService, 
    private sls: ShoppingListService,
    private activatedRoute: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.subscription = this.activatedRoute.params.subscribe( //subscribe lauscht sozusagen und reagiert auf Veränderungen
      params => {
        this.recipeId = +params['id']; //+params zu number konventieren (ursprünglich string); 'id' ist identisch zu Parameter id bei Routes
        this.selectedRecipe = this.recipeService.getRecipe(this.recipeId) //Ein Rezept bekommen an der Stelle id
      });
      
      
    
    /* (wird nicht mehr benötigt seit Routing)
    this.recipeService.recipeSelected.subscribe(
      (recipe: Recipe) => this.selectedRecipe = recipe
    ); */
  }

  onEdit() {
    this.router.navigate(['/rezepte', this.recipeId, 'bearbeiten']);
  }

  onAddToList(){
    console.log(this.sls);
    this.sls.addIngredients(this.selectedRecipe.ingredients);
  }

  onDelete(){
    this.router.navigate(['/rezepte']);
    this.recipeService.deleteRecipe(this.recipeId);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}

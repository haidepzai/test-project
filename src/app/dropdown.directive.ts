import { Directive, HostBinding, HostListener } from "@angular/core";

@Directive({
    selector: '[appDropdown]'
})
export class DropdownDirective {
    //class open anhängen (damit Dropdown geöffnet wird)
    @HostBinding('class.open') isOpen = false;

    @HostListener('click') toggle() {
        this.isOpen = !this.isOpen;
    }
}
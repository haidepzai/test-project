import { RecipeComponent } from './recipe/recipe.component';
import { Routes, RouterModule } from '@angular/router';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';


const APP_ROUTES: Routes = [
    { path: '', redirectTo: '/rezepte', pathMatch: 'full'}, //Ganze URL muss übereinstimmen; pathMatch: full = komplett leer
    //{ path: 'rezepte', component: RecipeComponent, children: RECIPE_ROUTES}, 
    //Verschachtelte Routes (Kind Routes) dh /rezepte/children
    { path: 'einkaufsliste', component: ShoppingListComponent}
];

export const routing = RouterModule.forRoot(APP_ROUTES);
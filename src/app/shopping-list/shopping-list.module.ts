import { FormsModule } from '@angular/forms';
import { ShoppingListComponent } from './shopping-list.component';
import { ShoppingListAddComponent } from './shopping-list-add.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { shoppingListRouting } from './shopping-list.routing';

@NgModule({
    declarations:[
        ShoppingListAddComponent,
        ShoppingListComponent
    ],
    imports: [
        FormsModule,
        CommonModule, //BrowserModule in Feature Module nicht verwenden, stattdessen CommonModule (beinhaltet ngIf, ngFor usw)
        shoppingListRouting
    ]
})
export class ShoppingListModule {}
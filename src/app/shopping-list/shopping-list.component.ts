import { ShoppingListService } from './shopping-list.service';
import { Component, OnInit } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styles: [
  ]
})
export class ShoppingListComponent implements OnInit {
  ingredients: Ingredient[] = [];
  selectedIngredient: Ingredient; //Soll an shopping-list-add übergeben werden

  constructor(private sls: ShoppingListService) { }

  onSelectItem(ingredient: Ingredient){
    this.selectedIngredient = ingredient;
  }

  onCleared(){
    this.selectedIngredient = null;
  }

  ngOnInit(): void {
    this.ingredients = this.sls.getIngredients();
  }

}

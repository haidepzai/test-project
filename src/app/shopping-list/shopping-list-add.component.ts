import { ShoppingListService } from './shopping-list.service';
import { Ingredient } from './../shared/ingredient.model';
import { Component, Input, OnChanges, OnInit, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-shopping-list-add',
  templateUrl: './shopping-list-add.component.html',
  styles: [`
    .invalidInput {
      border: 1px solid red;
    }
  `
  ]
})
export class ShoppingListAddComponent implements OnInit, OnChanges {
  /*selectedIngredient ist am Anfang undefined (Object existiert nicht)
  -> Leer und beim Ausfüllen der Felder überschreibt nicht die Property 
  und das Form wird nicht geupdated. Lösung: OnChanges*/

  @Input() selectedIngredient: Ingredient;
  @Output() cleared = new EventEmitter();
  isAdd = true;

  constructor(private sls: ShoppingListService) { }

  onSubmit(form: NgForm) {
    const newIngredient = new Ingredient(form.value.name, form.value.amount);
    console.log(form);
    if(!this.isAdd){
      //Bearbeiten
      this.sls.editIngredient(this.selectedIngredient, newIngredient);
    } else {
      //Neu
      this.sls.addIngredient(newIngredient);
    }
    this.onClear(form); //Nur die Form wird gecleared, aber nicht das selectedIngredient -> @Output() cleared
  }

  onClear(form: NgForm){
    //Parent Component mitteilen, dass gecleared wurde (Parent setzt selectedIngredient wieder auf null)
    this.cleared.emit(); 
    form.resetForm(); //Form Resetten
  }

  onDelete(form: NgForm){
    this.sls.deleteIngredient(this.selectedIngredient);
    this.onClear(form);
  }

  //So existiert Object mit Standardwerte 0
  //Falls die Werte von selectedIngredient null sind
  //->Dessen Werte auf null setzen (Add Modus)
  ngOnChanges(changes){ //reagiert auf Veränderungen
    if(changes.selectedIngredient.currentValue == null) {
      this.selectedIngredient = {name: null, amount: null};
      //Anfang war selectedIngredient null, jetzt nur noch die Felder
      this.isAdd = true;
    } else {
      this.isAdd = false; //Edit Modus
    }
  }

  ngOnInit(): void {
  }

}

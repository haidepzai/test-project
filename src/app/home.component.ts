import { Recipe } from './recipe/recipe.model';
import { Component, OnInit } from '@angular/core';
import { RecipeService } from './recipe/recipe.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styles: [`
    .card {
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
        width: 100%;
      }
      
      .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
      }
    `
    ]
})
export class HomeComponent implements OnInit {

    recipes : Recipe[];
    recipe: Recipe;

    constructor(private recipeService: RecipeService){

    }

    ngOnInit(){
        this.recipes = this.recipeService.getRecipes();
        //Auf das EventEmitter subscriben und so auf Veränderungen reagieren
        //(Wenn Rezept neu geladen wird)
        this.recipeService.recipesChanged.subscribe(
        (recipes: Recipe[]) => this.recipes = recipes
        )
        this.recipe = this.recipes[Math.floor(Math.random() * this.recipes.length)];
    }

}
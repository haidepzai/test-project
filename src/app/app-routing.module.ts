import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';

const routes: Routes = [
  //Jede URL würde path: '' matchen
  { path: '', component: HomeComponent}, 
  { 
    path: 'rezepte', //LazyLoad
    loadChildren: () => import('./recipe/recipe.module').then(m =>
        m.RecipeModule)
  }, 
  { 
    path: 'einkaufsliste', 
    loadChildren: () => import('./shopping-list/shopping-list.module').then(m =>
      m.ShoppingListModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
